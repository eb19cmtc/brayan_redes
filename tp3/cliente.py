import socket

ClientSocket = socket.socket()
host = input('Ingrese la direcion del servidor :')
port_p = input('ingrese el puerto :')
port = int (port_p)

print('Esperando por la conexion')
try:
    ClientSocket.connect((host, port))
except socket.error as e:
    print(str(e))

Response = ClientSocket.recv(1024)
while True:
    Input = input('Dices: ')
    ClientSocket.send(str.encode(Input))
    Response = ClientSocket.recv(1024)
    print(Response.decode('utf-8'))

ClientSocket.close()

