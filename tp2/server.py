import socket

mi_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ipServer = ('127.0.0.1', 8080)
mi_socket.bind(ipServer)
mi_socket.listen(5)

while True:
    print ("Esperando conexion")
    conexion, client_address = mi_socket.accept()
    try:
        print('cliente conectado;',client_address)
        while True:
            data = conexion.recv(16)
            print('Recibido {!r}'.format(data))
            if data:
                print('enviando datos devuelta al cliente')
                conexion.sendall(data)
            else:
                print('no datos de',client_address)
                break
    finally:
        conexion.close()

