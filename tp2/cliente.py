import socket

mi_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)



direcion_ip = input('Ingrese la direcion del servidor :')
direcion_puerto = input('ingrese el puerto :')
direcion_puerto = int (direcion_puerto)
direcion_del_server = (direcion_ip,direcion_puerto)

print('connecting to {} port {}'.format(*direcion_del_server))

mi_socket.connect(direcion_del_server)



try:
    message = b'This is the message.  It will be repeated.'
    print('sending {!r}'.format(message))
    mi_socket.sendall(message)

    amount_received = 0
    amount_expected = len(message)
    while amount_received < amount_expected:
        data = mi_socket.recv(16)
        amount_received += len(data)
        print('received {!r}'.format(data))

finally:
    print('cerrando')
    mi_socket.close()
